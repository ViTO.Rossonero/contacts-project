package com.example.vitorossonero.omertextesttask;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Patterns;
import android.webkit.URLUtil;
import android.widget.Button;

import java.net.URL;
import java.util.regex.Pattern;

/**
 * Custom EditTextPreference that will paste url from clipboard and prevent entering string that is not url
 * <p/>
 * Created by ViTO.Rossonero on 2015-03-01.
 */
public class CustomEditTextPreference extends EditTextPreference {
    private Context context;

    public CustomEditTextPreference(Context ctx, AttributeSet attrs, int defStyle) {
        super(ctx, attrs, defStyle);
        context = ctx;
    }

    public CustomEditTextPreference(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
        context = ctx;
    }

    private class EditTextWatcher implements TextWatcher {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            onEditTextChanged();
        }
    }

    EditTextWatcher m_watcher = new EditTextWatcher();

    /**
     * Return true in order to enable positive button or false to disable it.
     */
    protected boolean onCheckValue(String value) {
        if (TextUtils.isEmpty(value)) {
            return false;
        }
        Pattern URL_PATTERN = Patterns.WEB_URL;
        boolean isURL = URL_PATTERN.matcher(value).matches();
        if (!isURL) {
            String urlString = value + "";
            if (URLUtil.isNetworkUrl(urlString)) {
                try {
                    new URL(urlString);
                    isURL = true;
                } catch (Exception e) {
                }
            }
        }
        return isURL;
    }

    protected void onEditTextChanged() {
        boolean enable = onCheckValue(getEditText().getText().toString());
        Dialog dlg = getDialog();
        if (dlg instanceof AlertDialog) {
            AlertDialog alertDlg = (AlertDialog) dlg;
            Button btn = alertDlg.getButton(AlertDialog.BUTTON_POSITIVE);
            btn.setEnabled(enable);
        }
    }

    @Override
    protected void showDialog(Bundle state) {
        super.showDialog(state);
        getEditText().removeTextChangedListener(m_watcher);
        getEditText().addTextChangedListener(m_watcher);
//        getEditText().setText(((ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE)).getText());
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        if (clipboardManager.hasPrimaryClip()) {
            ClipData.Item item = clipboardManager.getPrimaryClip().getItemAt(0);
            if (item != null) {
                CharSequence c = item.getText();
                if (c != null && onCheckValue(c.toString())){
                    getEditText().setText(c.toString());
                }
            }
        }
        onEditTextChanged();
    }
}

package com.example.vitorossonero.omertextesttask.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.example.vitorossonero.omertextesttask.App.AppController;
import com.example.vitorossonero.omertextesttask.Model.Contact;
import com.example.vitorossonero.omertextesttask.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link ContactInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactInfoFragment extends Fragment {

    TextView tvName, tvAge, tvAddress, tvPhone, tvEmail;
    NetworkImageView nivPicture;

    private Contact contact;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param c contact to show.     *
     * @return A new instance of fragment ContactInfoFragment.
     */
    public static ContactInfoFragment newInstance(Contact c) {
        ContactInfoFragment fragment = new ContactInfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(Contact.KEY, c);
        fragment.setArguments(args);
        return fragment;
    }

    public ContactInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            contact = getArguments().getParcelable(Contact.KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_contact_info, container, false);

        tvName = (TextView) rootView.findViewById(R.id.tvName);
        tvAge = (TextView) rootView.findViewById(R.id.tvAge);
        tvAddress = (TextView) rootView.findViewById(R.id.tvAddress);
        tvPhone = (TextView) rootView.findViewById(R.id.tvPhone);
        tvEmail = (TextView) rootView.findViewById(R.id.tvEmail);
        nivPicture = (NetworkImageView) rootView.findViewById(R.id.nivPicture);

        if (contact == null) {
            Log.d("ContactInfoFragment", "Something went wrong: contact = null");
        } else {
            tvName.setText(contact.getName());
            tvAge.setText(String.valueOf(contact.getAge()));
            tvAddress.setText(contact.getAddress());
            tvPhone.setText(contact.getPhone());
            tvPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + contact.getPhone()));
                    startActivity(callIntent);
                }
            });
            tvEmail.setText(contact.getEmail());
            tvEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", contact.getEmail(), null));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.emailSubject));
                    emailIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.emailText) + contact.getPhone());
                    startActivity(Intent.createChooser(emailIntent, getResources().getString(R.string.emailChooser)));
                }
            });

            nivPicture.setDefaultImageResId(R.drawable.no_photo);
            nivPicture.setErrorImageResId(R.drawable.no_photo_error);
            nivPicture.setImageUrl("http://" + contact.getPicture(), AppController.getInstance().getImageLoader());
        }
        return rootView;
    }
}

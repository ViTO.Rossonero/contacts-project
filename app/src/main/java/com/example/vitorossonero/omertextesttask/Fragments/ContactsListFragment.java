package com.example.vitorossonero.omertextesttask.Fragments;

import android.app.Activity;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.vitorossonero.omertextesttask.App.AppController;
import com.example.vitorossonero.omertextesttask.Model.Contact;
import com.example.vitorossonero.omertextesttask.R;
import com.example.vitorossonero.omertextesttask.Utils.JsonArrayRequestWithCache;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * <p/>
 * Activities containing this fragment MUST implement the {@link com.example.vitorossonero.omertextesttask.Fragments.ContactsListFragment.OnContactSelectedListener}
 * interface.
 */
public class ContactsListFragment extends ListFragment {

    //    private static final String url = "http://www.json-generator.com/api/json/get/bTwQKvxGbm?indent=2";
//    private static final String url = "http://10.0.3.2:8080/contacts/1.json";
    public static final String SERVER_URL_KEY = "server_url";
    public static boolean serverChanged = false;

    private static final String STATE_ACTIVATED_POSITION = "activated_position";
    private static final String TAG = ContactsListFragment.class.getSimpleName();
    private final String tag_json_array = "json_array_req";

    private static String url = "";
    private List<Contact> contacts;
    private int mActivatedPosition = ListView.INVALID_POSITION;
    private boolean isDualPane = false;

    private OnContactSelectedListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ContactsListFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnContactSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnContactSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            mActivatedPosition = savedInstanceState.getInt(STATE_ACTIVATED_POSITION);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity().findViewById(R.id.contacts_info_container) != null) {
            isDualPane = true;
        }
        setEmptyText(getString(R.string.no_data));
        fetchContacts(false);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (serverChanged) {
            fetchContacts(true);
            serverChanged = false;
        }
    }

    /**
     * fetch contacts from server or from cache
     *
     * @param disabledCache whether to try to fetch from server
     */
    public void fetchContacts(final boolean disabledCache) {
        url = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(SERVER_URL_KEY, "");
        if (disabledCache) {
            AppController.getInstance().getRequestQueue().getCache().invalidate(url, false);
        }

        JsonArrayRequest req = new JsonArrayRequestWithCache(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        onFetchContactsResponse(response, disabledCache);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onFetchContactsError(error, disabledCache);
            }
        });
        AppController.getInstance().addToRequestQueue(req, tag_json_array);
    }

    /**
     * Processing response
     *
     * @param response      response
     * @param disabledCache whether there was attempt to fetch contacts from server
     */
    private void onFetchContactsResponse(JSONArray response, boolean disabledCache) {
//        List<Contact> updatedContacts = parseJSON(response);
        Type conactListType = new TypeToken<List<Contact>>() {
        }.getType();
        List<Contact> updatedContacts = new Gson().fromJson(response.toString(), conactListType);
        Collections.sort(updatedContacts);

        if (contacts == null) {
//        onActivityCreated()
            contacts = updatedContacts;
            ListAdapter adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_activated_1, contacts);
            setListAdapter(adapter);

            if (!contacts.isEmpty() && mActivatedPosition == ListView.INVALID_POSITION) {
                mActivatedPosition = 0;
            }
            setActivatedPosition();
//            user clicked refresh
        } else if (!contacts.equals(updatedContacts)) {
//            contacts != updatedContacts ==> New contacts have been uploaded(or order of contacts changed[look at List.equals(Object o)])
            contacts = updatedContacts;
            ListAdapter adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_activated_1, contacts);
            setListAdapter(adapter);
            Toast.makeText(getActivity(), getResources().getString(R.string.contacts_refreshed), Toast.LENGTH_SHORT).show();
            mActivatedPosition = (updatedContacts.isEmpty()) ? ListView.INVALID_POSITION : 0;
            setActivatedPosition();
        } else {
            if (disabledCache) {
                Toast.makeText(getActivity(), getResources().getString(R.string.contacts_refreshed), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void onFetchContactsError(VolleyError error, boolean disabledCache) {
        Log.d(TAG, "onFetchContactsError: " + error.getMessage());
        Toast.makeText(getActivity(), getResources().getString(R.string.fetchError), Toast.LENGTH_LONG).show();
        if (contacts == null && !disabledCache) {
            contacts = new ArrayList<>();
            ListAdapter adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_activated_1, contacts);
            setListAdapter(adapter);
        }
    }

    private void setActivatedPosition() {
        if (isDualPane && mActivatedPosition != ListView.INVALID_POSITION) {
            mListener.onContactSelected(contacts.get(mActivatedPosition));
            getListView().setItemChecked(mActivatedPosition, true);
        }
    }

    //        TODO try Gson instead
    private List<Contact> parseJSON(JSONArray jsonArray) {
        List<Contact> contactList = new ArrayList<>(jsonArray.length());
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject c = jsonArray.getJSONObject(i);

                String name = c.getString(Contact.TAG_NAME);
                int age = c.getInt(Contact.TAG_AGE);
                String address = c.getString(Contact.TAG_ADDRESS);
                String phone = c.getString(Contact.TAG_PHONE);
                String email = c.getString(Contact.TAG_EMAIL);
                String picture = c.getString(Contact.TAG_PICTURE);

                contactList.add(new Contact(name, age, address, phone, email, picture));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return contactList;
    }

    public void setActivateOnItemClick(boolean activateOnItemClick) {
        getListView().setChoiceMode(activateOnItemClick ? ListView.CHOICE_MODE_SINGLE : ListView.CHOICE_MODE_NONE);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if (null != mListener && (!isDualPane || mActivatedPosition != position)) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onContactSelected(contacts.get(position));
            mActivatedPosition = position;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnContactSelectedListener {
        public void onContactSelected(Contact contact);
    }
}

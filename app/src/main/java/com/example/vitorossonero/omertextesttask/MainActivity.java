package com.example.vitorossonero.omertextesttask;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.vitorossonero.omertextesttask.Fragments.ContactInfoFragment;
import com.example.vitorossonero.omertextesttask.Fragments.ContactsListFragment;
import com.example.vitorossonero.omertextesttask.Model.Contact;


public class MainActivity extends ActionBarActivity implements ContactsListFragment.OnContactSelectedListener {

    public static final String TAG = MainActivity.class.getSimpleName();
    private boolean mDualPane = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
        if (findViewById(R.id.contacts_info_container) != null) {
            mDualPane = true;
            ((ContactsListFragment) getSupportFragmentManager().findFragmentById(R.id.contactsListFragment)).setActivateOnItemClick(true);
        }
    }

    @Override
    public void onContactSelected(Contact contact) {
        if (mDualPane) {
            Fragment fragment = ContactInfoFragment.newInstance(contact);
            getSupportFragmentManager().
                    beginTransaction().
                    replace(R.id.contacts_info_container, fragment).
                    commit();
//            TODO do it every time after commit but only on UI thread
            getSupportFragmentManager().executePendingTransactions();
        } else {
            Intent i = new Intent(this, ContactInfoActivity.class);
            Bundle b = new Bundle();
            b.putParcelable(Contact.KEY, contact);
            i.putExtras(b);
            startActivity(i);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()){
            case R.id.mRefresh:
//                TODO check this line
                ((ContactsListFragment) getSupportFragmentManager().findFragmentById(R.id.contactsListFragment)).fetchContacts(true);
                return true;
            case R.id.action_settings:
                startActivity(new Intent(this,SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

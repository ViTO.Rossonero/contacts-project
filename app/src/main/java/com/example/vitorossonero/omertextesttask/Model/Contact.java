package com.example.vitorossonero.omertextesttask.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ViTO.Rossonero on 2015-02-25.
 */
public class Contact implements Parcelable, Comparable<Contact> {
    public static final String TAG_NAME = "name";
    public static final String TAG_AGE = "age";
    public static final String TAG_ADDRESS = "address";
    public static final String TAG_PHONE = "phone";
    public static final String TAG_EMAIL = "email";
    public static final String TAG_PICTURE = "picture";

    public static final String KEY = "contact";
//    private final String scheme = "http://";

    private String name;
    private int age;
    private String address;
    private String phone;
    private String email;
    private String picture;

    public Contact(String name, int age, String address, String phone, String email, String picture) {
        this.name = name;
        this.age = age;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.picture = picture;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public int compareTo(Contact another) {
        return name.compareTo(another.getName());
    }

    //    TODO can use EqualsBuilder and HashCodeBuilder from the Apache Commons Lang library or look at Guava
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contact contact = (Contact) o;

        if (age != contact.age) return false;
        if (address != null ? !address.equals(contact.address) : contact.address != null)
            return false;
        if (email != null ? !email.equals(contact.email) : contact.email != null) return false;
        if (name != null ? !name.equals(contact.name) : contact.name != null) return false;
        if (phone != null ? !phone.equals(contact.phone) : contact.phone != null) return false;
        if (picture != null ? !picture.equals(contact.picture) : contact.picture != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (picture != null ? picture.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(age);
        dest.writeString(address);
        dest.writeString(phone);
        dest.writeString(email);
        dest.writeString(picture);
    }

    public static final Parcelable.Creator CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel source) {
            return new Contact(source);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    private Contact(Parcel in){
        name = in.readString();
        age = in.readInt();
        address = in.readString();
        phone = in.readString();
        email = in.readString();
        picture = in.readString();
    }
}
